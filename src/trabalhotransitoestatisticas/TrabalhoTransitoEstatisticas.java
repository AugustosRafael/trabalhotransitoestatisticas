/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhotransitoestatisticas;

import java.io.IOException;
import javax.swing.JOptionPane;

public class TrabalhoTransitoEstatisticas {

    public static void main(String[] args) throws IOException {
        
        int Matriz[][] = new int[15][15];
        int menu = 0;
        String  Erro = "";
        
        ParametosEstatisticas[] pa = new ParametosEstatisticas[15];
        Metodos metodo = new Metodos();
        
        
        
        while(menu != 9){
            menu = Integer.parseInt(JOptionPane.showInputDialog(null,"              Estatisticas de acidente 2015 \n \n"
                    + "1- Cadastro de estatística \n"
                    + "2- Consulta por tipo de veículo \n"
                    + "3- Consulta por quantidade de acidentes \n"
                    + "4- Consulta todas as cidades \n"
                    + "5- Consulta maior, menor, média de acidentes \n"
                    + "9- Sair \n \n"
                    + Erro,"Menu Estatísticas \n",-1));
            
            switch(menu){
                case 1:
                    pa = metodo.FCADRASTRAESTATISTICA(pa);
                    Erro = "";
                    break;
                case 2:
                    metodo.FTIPO(pa);
                    Erro = "";
                    break;
                case 3:
                    metodo.PQTACIDENTES(pa);
                    Erro = "";
                    break;
                case 4:
                    metodo.PCONSULTAACIDENTES(pa);
                    break;
                case 5:
                    metodo.GRAFICO(pa);
                    break;
                case 9:
                    System.exit(0);
                    break;
                default:
                    Erro = "Erro 505- Por Favor Digite Um Número Válido!!";
                    break;
            }
        }
    }
    
}
