/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhotransitoestatisticas;

import java.awt.Color;
import java.awt.Font;
import java.io.*;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.plaf.FontUIResource;

public class Metodos{
    
    public ParametosEstatisticas[] FCADRASTRAESTATISTICA(ParametosEstatisticas[] ParaEsta) throws IOException{ 
        
        int cont = 0, Correcao;
        BufferedWriter writer = new BufferedWriter(new FileWriter("Estatisticas.txt"));
        
        for(cont = 0; cont < 15; cont++){
            ParaEsta[cont] = new ParametosEstatisticas();
        }
         cont = 0;
        for(cont = 0; cont < 15; cont++){
            /*ParaEsta[cont].NomeCidade = JOptionPane.showInputDialog(null, "Nome da cidade: ","Cadastro",-1);
            writer.write(ParaEsta[cont].NomeCidade);
            writer.newLine();*/
            Correcao = 7;
            while(Correcao == 7){
                ParaEsta[cont].CodCidade = Integer.parseInt(JOptionPane.showInputDialog(null,"0-São Paulo \n"
                    + "1- Osasco \n"
                    + "2- Guaruhos \n"
                    + "3- Barueri \n"
                    + "4- Embu Das Artes \n"
                    + "Escolha Cidade:","Cadastro",-1));
                if((ParaEsta[cont].TipoVeiculo < 0) || (ParaEsta[cont].TipoVeiculo > 6)){
                    Correcao = 7;
                }else{
                    Correcao = 1;
                }
            }
            switch(ParaEsta[cont].CodCidade){
                case 0:
                    ParaEsta[cont].NomeCidade = "Sao Paulo";
                    break;
                case 1:
                    ParaEsta[cont].NomeCidade = "Osasco";
                    break;
                case 2:
                    ParaEsta[cont].NomeCidade = "Guarulhos";
                    break;
                case 3:
                    ParaEsta[cont].NomeCidade = "Barueri";
                    break;
                case 4:
                    ParaEsta[cont].NomeCidade = "Embu Das Artes";
                    break;
            }
            writer.write(ParaEsta[cont].NomeCidade);
            writer.newLine();
            writer.write(Integer.toString(ParaEsta[cont].CodCidade));
            writer.newLine();
            ParaEsta[cont].QuantAcidente = Integer.parseInt(JOptionPane.showInputDialog(null,"Quantidade de Acidentes:","Cadastro",-1));
            writer.write(Integer.toString(ParaEsta[cont].QuantAcidente));
            writer.newLine();
            ParaEsta[cont].TipoVeiculo = 7;
            while(ParaEsta[cont].TipoVeiculo == 7){
                ParaEsta[cont].TipoVeiculo = Integer.parseInt(JOptionPane.showInputDialog(null,"0-Carro \n"
                    + "1- Caminhão \n"
                    + "2- Ônibus \n"
                    + "3- Moto \n"
                    + "4- Bicicleta \n"
                    + "Tipo de Veículo:","Cadastro",-1));
                if((ParaEsta[cont].TipoVeiculo < 0) || (ParaEsta[cont].TipoVeiculo > 6)){
                    ParaEsta[cont].TipoVeiculo = 7;
                }
            }    
            writer.write(Integer.toString(ParaEsta[cont].TipoVeiculo));
            writer.newLine();
            
        }
        writer.close();
        
    return ParaEsta;}

    public void FTIPO(ParametosEstatisticas[] ParaEsta) throws IOException{
        
        int cont, SomaGeralAciden = 0;
        float Porcentagem;
        int TiposVeiculo[] = new int[5];
        String Relatorio = "",Nome = "", RelatorioEspecifico = "", Barra="";
        BufferedReader ler = new BufferedReader(new FileReader("Estatisticas.txt"));
        
        for (cont = 0; cont < 15; cont++){
            ParaEsta[cont] = new ParametosEstatisticas();
        }
        
        for (cont = 0; cont < 15; cont++){
            ParaEsta[cont].NomeCidade =(ler.readLine());
            ParaEsta[cont].CodCidade = Integer.parseInt(ler.readLine());
            ParaEsta[cont].QuantAcidente = Integer.parseInt(ler.readLine()); 
            ParaEsta[cont].TipoVeiculo = Integer.parseInt(ler.readLine());
            TiposVeiculo[ParaEsta[cont].TipoVeiculo] += ParaEsta[cont].QuantAcidente;
            SomaGeralAciden += ParaEsta[cont].QuantAcidente;
        }
        
        for (cont = 0; cont < 5; cont++){
            switch(cont){
                case 0:
                    Nome = "Carro";
                    break;
                case 1:
                    Nome = "Caminhão";
                    break;
                case 2:
                    Nome = "Ônibus";
                    break;
                case 3:
                    Nome = "Moto";
                    break;
                case 4:
                    Nome = "Bicicleta";
                    break;
            }
            
            Porcentagem =(  TiposVeiculo[cont] * 100 ) / SomaGeralAciden; 
            for(int ContPor = 0; ContPor < Porcentagem; ContPor +=2){
                Barra +="I";
            }

            
            Relatorio += "    - "+ Nome+": " + TiposVeiculo[cont] +"º acidente(s) Percentual: "+Barra+" "+Porcentagem+"%  \n \n";
            
            Porcentagem = 0;
            Barra = "";
            
            
        }
        
        Object[] options = {"Carro","Caminhão","Ônibus","Moto","Bicicleta"};
        int CodigoVeiculo = JOptionPane.showOptionDialog(null,Relatorio, "Consulta - Por Tipo de veículo"
                + "", JOptionPane.DEFAULT_OPTION,-1, null,  options,null);
        
        
        for (cont = 0; cont < 15; cont++){
            if(ParaEsta[cont].TipoVeiculo == CodigoVeiculo){
                RelatorioEspecifico += "Nome Cidade: " + ParaEsta[cont].NomeCidade+"    |    ";
                RelatorioEspecifico += "Código da Cidade: " +Integer.toString(ParaEsta[cont].CodCidade)+"    |    ";
                RelatorioEspecifico += "Quantidade de Acidente(s): " + Integer.toString(ParaEsta[cont].QuantAcidente)+"    |    "; 
                RelatorioEspecifico += "Tipo de veículo: " + Integer.toString(ParaEsta[cont].TipoVeiculo)+" \n \n";
            }      
        }
        JOptionPane.showMessageDialog(null, RelatorioEspecifico ,"Relatório - Veículo",-1);
        ler.close();
        
    }
    
    public void PQTACIDENTES(ParametosEstatisticas[] ParaEsta) throws IOException{
        int cont,Num = 0,NumDois = 0;
        char ValidarMen = 1;
        String Relatorio=""; 
        BufferedReader ler = new BufferedReader(new FileReader("Estatisticas.txt"));
        Object[] options = {"Padrão","Customizada"};
        
        int CodigoPesquisa = JOptionPane.showOptionDialog(null,"Padão - entre 100 e 500 \n"
                + "Customizado - Valor que quiser", "Consulta - Por Quantidade De Acidentes"
                + "", JOptionPane.DEFAULT_OPTION,-1, null,  options,null);
        
        for (cont = 0; cont < 15; cont++){
            ParaEsta[cont] = new ParametosEstatisticas();
        }
        
        for (cont = 0; cont < 15; cont++){
            ParaEsta[cont].NomeCidade =(ler.readLine());
            ParaEsta[cont].CodCidade = Integer.parseInt(ler.readLine());
            ParaEsta[cont].QuantAcidente = Integer.parseInt(ler.readLine()); 
            ParaEsta[cont].TipoVeiculo = Integer.parseInt(ler.readLine());
            
        }
        
        if(CodigoPesquisa == 0){
            for (cont = 0; cont < 15; cont++){
                if((ParaEsta[cont].QuantAcidente > 100) && (ParaEsta[cont].QuantAcidente < 500)){
                    Relatorio += "Nome Cidade: " + ParaEsta[cont].NomeCidade+"    |    Código Da Cidade: "
                            + ""+ParaEsta[cont].CodCidade + "    |    Quantidade De Acidentes: " + ParaEsta[cont].QuantAcidente+""
                            + "    |    Tipo Veículo: " + ParaEsta[cont].TipoVeiculo+" \n \n" ;
                }
            }
            JOptionPane.showMessageDialog(null, Relatorio,"Consulta - Por Quantidade De Acidentes",-1);
        }else{
            

        try {
            Num = Integer.parseInt(JOptionPane.showInputDialog(null,"Digite o intervalo desejado (Inicial): \n"
                        + "","Consulta - Por Quantidade De Acidentes",-1));
           
        } catch(NumberFormatException nfe) {
             JOptionPane.showMessageDialog(null, "505 Erro - Valor Digitado Inválido");
              ValidarMen = 0;
        }
        
        if(ValidarMen == 1){
            try {
                NumDois = Integer.parseInt(JOptionPane.showInputDialog(null,"Digite o intervalo desejado (Final): \n"
                            + "","Consulta - Por Quantidade De Acidentes",-1));
            } catch(NumberFormatException nfe) {
                 JOptionPane.showMessageDialog(null, "505 Erro - Valor Digitado Inválido");
                  ValidarMen = 0;
            }
        }

                
            Relatorio = "";
            for (cont = 0; cont < 15; cont++){
                if((ParaEsta[cont].QuantAcidente > Num) && (ParaEsta[cont].QuantAcidente < NumDois)){
                    Relatorio += "Nome Cidade: " + ParaEsta[cont].NomeCidade+"    |    Código Da Cidade: "
                                + ""+ParaEsta[cont].CodCidade + "    |    Quantidade De Acidentes: " + ParaEsta[cont].QuantAcidente+""
                                + "    |    Tipo Veículo: " + ParaEsta[cont].TipoVeiculo +" \n \n"  ;
                }
            }
            if(Relatorio.equals("") && ValidarMen == 1){
                JOptionPane.showMessageDialog(null, "Não Possui Dados!!","Consulta - Por Quantidade De Acidentes",-1);
            }else if(ValidarMen == 1){
                JOptionPane.showMessageDialog(null, Relatorio,"Consulta - Por Quantidade De Acidentes",-1);
            }
            
        }
    }
    
   public void PCONSULTAACIDENTES(ParametosEstatisticas[] ParaEsta) throws  IOException{
       int Cod = 0,cont;
       char ValidarMen = 1;
       String Relatorio = "";
       BufferedReader ler = new BufferedReader(new FileReader("Estatisticas.txt"));
       
       try {
            Cod = Integer.parseInt(JOptionPane.showInputDialog(null,"Código da cidade: ","Consulta - Por Cidade",-1)); 
       } catch(NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, "505 Erro - Valor Digitado Inválido");
            ValidarMen = 0;
       }
       
       for (cont = 0; cont < 15; cont++){
            ParaEsta[cont] = new ParametosEstatisticas();
        }
        
        for (cont = 0; cont < 15; cont++){
            ParaEsta[cont].NomeCidade =(ler.readLine());
            ParaEsta[cont].CodCidade = Integer.parseInt(ler.readLine());
            ParaEsta[cont].QuantAcidente = Integer.parseInt(ler.readLine()); 
            ParaEsta[cont].TipoVeiculo = Integer.parseInt(ler.readLine());
            
        }
        
        for (cont = 0; cont < 15; cont++){
                if(ParaEsta[cont].CodCidade == Cod){
                    Relatorio += "Nome Cidade: " + ParaEsta[cont].NomeCidade+"    |    Código Da Cidade: "
                                + ""+ParaEsta[cont].CodCidade + "    |    Quantidade De Acidentes: " + ParaEsta[cont].QuantAcidente+""
                                + "    |    Tipo Veículo: " + ParaEsta[cont].TipoVeiculo +" \n \n"  ;
                }
        }
        if(Relatorio.equals("") && ValidarMen == 1){
            JOptionPane.showMessageDialog(null, "Não Possui Dados!!","Consulta - Por Cidade",-1);
        }else if(ValidarMen == 1){
            JOptionPane.showMessageDialog(null, Relatorio,"Consulta - Por Cidade",-1);
        }
        
    }
   
    
    public void GRAFICO(ParametosEstatisticas[] ParaEsta) throws IOException{
       int Cod = 0,cont,TotalAcidentes = 0, QtdCidade = 0, Media, Maior = 0, Menor = 0;
       float Porcentagem = 0.0f;
       char ValidarMen = 1;
       String RelatorioCidade[][] = new String[3][5];
       String Relatorio = "", NomeCidade="", Barra="";
       
       BufferedReader ler = new BufferedReader(new FileReader("Estatisticas.txt"));
       
       for (cont = 0; cont < 15; cont++){
            ParaEsta[cont] = new ParametosEstatisticas();
        }
        
        for (cont = 0; cont < 15; cont++){
            ParaEsta[cont].NomeCidade =(ler.readLine());
            ParaEsta[cont].CodCidade = Integer.parseInt(ler.readLine());
            ParaEsta[cont].QuantAcidente = Integer.parseInt(ler.readLine()); 
            ParaEsta[cont].TipoVeiculo = Integer.parseInt(ler.readLine());
            TotalAcidentes += ParaEsta[cont].QuantAcidente;
                  
        }
        
        
        
        for(cont = 0; cont < 5; cont++){
            
            RelatorioCidade[0][cont] = Integer.toString(cont); 
            switch(cont){
                case 0:
                    RelatorioCidade[1][cont] ="São Paulo";
                    break;
                case 1:
                    RelatorioCidade[1][cont] ="Osasco";
                    break;
                case 2:
                    RelatorioCidade[1][cont] ="Guarulhos";
                    break;
                case 3:
                    RelatorioCidade[1][cont] ="Barueri";
                    break;
                case 4:
                    RelatorioCidade[1][cont] ="Embu Das Artes";
                    break;
                    
            }
            RelatorioCidade[2][cont] = "0";   
                
            for(int contdois = 0; contdois < 15; contdois++){
                if(RelatorioCidade[0][cont].equals(Integer.toString(ParaEsta[contdois].CodCidade))){
                    RelatorioCidade[0][cont] = Integer.toString(cont); 
                    RelatorioCidade[1][cont] = ParaEsta[contdois].NomeCidade;
                    RelatorioCidade[2][cont] = Integer.toString((Integer.parseInt(RelatorioCidade[2][cont]) + ParaEsta[contdois].QuantAcidente));   
                }
            }
            
        }
                    
        for(cont = 0; cont < 5; cont++){
            if(cont == 0){
                Menor = Integer.parseInt(RelatorioCidade[2][cont]);
                Maior = Integer.parseInt(RelatorioCidade[2][cont]);
            }else{
                if(Maior < (Integer.parseInt(RelatorioCidade[2][cont]))){
                    Maior = Integer.parseInt(RelatorioCidade[2][cont]);
                }else if(Menor > (Integer.parseInt(RelatorioCidade[2][cont]))){
                    Menor = Integer.parseInt(RelatorioCidade[2][cont]);
                }
            }

            Relatorio += "Nome Cidade: "+RelatorioCidade[1][cont] + " \n";
            Relatorio += "Quantidade de Acidentes: "+RelatorioCidade[2][cont] + " \n";
            
            Porcentagem =(Integer.parseInt(RelatorioCidade[2][cont]) * 100 ) / TotalAcidentes;
            for(int contpor = 0; contpor < Porcentagem; contpor +=2){
                Barra +="I";
            }
            
            Relatorio += "Percentual De Acidentes: "+ Barra +" "+ Porcentagem + "% \n";
            Relatorio += "---------------------------------------------------------------------------------------- \n";
            if(cont == 4){
                Relatorio += "                                           Relatório                                             \n";
                Media = TotalAcidentes / 5;
                Relatorio +="Cidades Acima Da Média: \n";
                if(Media < (Integer.parseInt(RelatorioCidade[2][0]))){
                    Relatorio +="  - "+RelatorioCidade[1][0] +"\n";
                }
                if(Media < (Integer.parseInt(RelatorioCidade[2][1]))){
                    Relatorio +="  - "+RelatorioCidade[1][1] +"\n";
                }
                if(Media < (Integer.parseInt(RelatorioCidade[2][2]))){
                    Relatorio +="  - "+RelatorioCidade[1][2] +"\n";
                }
                if(Media < (Integer.parseInt(RelatorioCidade[2][3]))){
                    Relatorio +="  - "+RelatorioCidade[1][3] +"\n";
                }
                if(Media < (Integer.parseInt(RelatorioCidade[2][4]))){
                    Relatorio +="  - "+RelatorioCidade[1][4] +"\n";
                }
                
                Relatorio +="Maior Nº De Acidentes: "+Maior+"\nMenor Nº De Acidentes: "+Menor+"\n\n";
                
            }
           
            Barra = "";
               
        }
       
        if(Relatorio.equals("")){
            JOptionPane.showMessageDialog(null, "Não Possui Dados!!","Consulta - Geral Das Cidades",-1);
        }else{
            JOptionPane.showMessageDialog(null, Relatorio,"Consulta - Geral Das Cidades",-1);
        }
        
        
    }
       
}
