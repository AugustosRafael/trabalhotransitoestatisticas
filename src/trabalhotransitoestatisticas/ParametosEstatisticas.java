
package trabalhotransitoestatisticas;

class ParametosEstatisticas {
    
    String NomeCidade;
    int CodCidade;
    int QuantAcidente;
    int TipoVeiculo;
    
    ParametosEstatisticas(){
        this.NomeCidade  = "";
        this.CodCidade = 0;
        this.QuantAcidente = 0;
        this.TipoVeiculo = 0;
        //this("",0,0,0);
    }

    ParametosEstatisticas(String NomeCidade, int CodCidade, int QuantAcidente, int TipoVeiculo) {
        this.NomeCidade = NomeCidade;
        this.CodCidade = CodCidade;
        this.QuantAcidente = QuantAcidente;
        this.TipoVeiculo = TipoVeiculo;
    }
    
    
    
}
